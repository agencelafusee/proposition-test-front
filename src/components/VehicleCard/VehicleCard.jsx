import styled from 'styled-components';

const VehicleContainer = styled.div`
  flex: 0 0 30%;
  border: 1px solid #000;
  border-radius: 0.25rem;
  padding: 0.5rem 1rem;
  margin-bottom: 0.5rem;
`;

let VehicleCard = (props) => {
  return (
    <VehicleContainer>
      <div>
        #{props.id} - {props.make_and_model} (
        {props.color})
      </div>
      {props.doors < 2 && <div>{props.doors} door</div>}
      {props.doors >= 2 && <div>{props.doors} doors</div>}
      <div>{props.mileage} miles</div>
    </VehicleContainer>
  );
};

export default VehicleCard;
