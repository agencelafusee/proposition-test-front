import { ThemeProvider } from "styled-components";
import VehiculeIndex from "./domains/Vehicles";
import theme from "./theme";

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <VehiculeIndex />
      </ThemeProvider>
    </div>
  );
}

export default App;
