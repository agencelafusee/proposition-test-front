import React, { useEffect, useState } from "react";
import VehicleCard from "../../components/VehicleCard/VehicleCard";
import { getVehicles } from "../../services/vehicles";
import styled from 'styled-components';

const ActionContainer = styled.div`
  margin-bottom: 1rem;
`;

const CardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

const VehiculeIndex = () => {
  const [cars, setCars] = useState(null);
  let order = false;

  useEffect(() => {
    const fetchCars = async () => {
      setCars(await getVehicles());
    };
    fetchCars();
  }, []);

  if (!cars) {
    return <>loading</>;
  }

  if (order) {
    return (
      <>
        <h3>My garage</h3>
        <ActionContainer>
          <button onClick={() => (order = false)}>Reset order</button>
        </ActionContainer>
        <CardContainer>
          {cars.map((car) => {
            if (car.color !== "Black" && car.color !== "White") return <> </>;
            return <VehicleCard key={car.id} {...car} />;
          })}
        </CardContainer>
      </>
    );
  }

  return (
    <>
      <h3>My garage</h3>
      <ActionContainer>
        <button
          style={{ border: "red 1px solid", cursor: "pointer" }}
          onClick={() => (order = true)}
        >
          Only black & white
        </button>
      </ActionContainer>
      <CardContainer>
        {cars.map((car) => (
          <VehicleCard key={car.id} {...car} />
        ))}
      </CardContainer>
    </>
  );
};

export default VehiculeIndex;
